# ClickHouse for Error Tracking on GitLab.com

## Summary

**Provide a high level summary of this new product feature. Explain how this change will benefit GitLab customers. Enumerate the customer use-cases.**


There is a growing need for a highly-scalable, resilient, and OLAP-capable datastore for GitLab.com. After discussion, ClickHouse was selected as the best fit in supporting key observability and analytic workloads, see [epic](https://gitlab.com/groups/gitlab-org/opstrace/-/epics/27) and [issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1632) for further background.

ClickHouse has the potential to power a number of workflows across the platform with the initial integration focused around a rollout to GitLab.com prior to deployment as a part of self-managed.

While there is a large scope of features which may come to rely on ClickHouse, the primary target will be enabling [Error Tracking](https://about.gitlab.com/handbook/engineering/development/ops/monitor/observability/#clickhouse-datastore) as part of the Observability feature category.

ClickHouse will be deployed as a component of the Opstrace stack and managed by Opstrace directly. Alongside the storage backend will be a golang service for both querying ClickHouse and ingesting incoming data to load into ClickHouse directly.

The term "feature launch" in this particular document is understood as the moment when the beta version of error tracking api is enabled in production.

**What metrics, including business metrics, should be monitored to ensure will this feature launch will be a success?**

As a new datastore, ClickHouse will be primarily monitored via performance metrics ensuring availability and usage are within expected parameters. See [documentation for monitoring overview](https://clickhouse.com/docs/en/operations/monitoring/).

The following are key errortracking API metrics to be monitored:
- Request latency
- Requests per second
- Error rate
- Saturation

The following are key Clickhouse metrics to be monitored:
- RSS/Mapped memory usage (via [system.asynchronous_metrics](https://clickhouse.com/docs/en/operations/system-tables/asynchronous_metrics/#system_tables-asynchronous_metrics))
- Disk usage (via [system.metrics](https://clickhouse.com/docs/en/operations/system-tables/metrics/#system_tables-metrics))
- Data volume to replicas (via [system.metrics](https://clickhouse.com/docs/en/operations/system-tables/metrics/#system_tables-metrics))
- Active background tasks (via [system.metrics](https://clickhouse.com/docs/en/operations/system-tables/metrics/#system_tables-metrics))
- Number of connections to HTTP server (via [system.metrics](https://clickhouse.com/docs/en/operations/system-tables/metrics/#system_tables-metrics))
- Total executing query count (via [system.metrics](https://clickhouse.com/docs/en/operations/system-tables/metrics/#system_tables-metrics))
- Total background merge count (via [system.metrics](https://clickhouse.com/docs/en/operations/system-tables/metrics/#system_tables-metrics))
- [Load on processors](https://clickhouse.com/docs/en/operations/monitoring/#resource-utilization)

## Architecture

**Add architecture diagrams to this issue of feature components and how they interact with existing GitLab components. Include internal dependencies, ports, security policies, etc.**

```mermaid
sequenceDiagram
  actor svc as Error tracking client service
  participant nginx
  participant gatekeeper
  participant errortracking
  participant clickhouse
  participant gitlab.com

  svc ->> nginx: POST error1 to <br> htpps://PUBLIC_KEY@opstrace.com/projects/api/:project_id/{envelope|store}
  Note over gitlab.com: GitLab Saas boundary
  Note over nginx,clickhouse: Opstrace realm boundary
  nginx ->> gatekeeper: GET /error_tracking/auth
  Note right of nginx: Validate PUBLIC_KEY and project_id pair
  gatekeeper ->> gitlab.com: POST /internal/error_tracking_allowed
  gitlab.com ->> gatekeeper: 200 Ok
  gatekeeper ->> nginx: 200 Ok
  nginx -->> errortracking: POST error1 to <br> projects/api/:project_id/{envelope|store}
  errortracking ->> clickhouse: INSERT error1
  clickhouse ->> errortracking: 200 Ok
  errortracking ->> nginx: 200 Ok
  nginx ->> svc: 200 Ok

  gitlab.com ->> gatekeeper: GET /projects/:project_id/errors
  gatekeeper ->> gitlab.com: 200 Ok

  gitlab.com ->> gatekeeper: GET /projects/:project_id/errors/:fingerprint
  gatekeeper ->> gitlab.com: 200 Ok

  gitlab.com ->> gatekeeper: GET /projects/:project_id/errors/:fingerprint/events
  gatekeeper ->> gitlab.com: 200 Ok

  gitlab.com ->> gatekeeper: PUT /projects/:project_id/errors/:fingerprint
  gatekeeper ->> gitlab.com: 200 Ok
```

Incoming structured data will be ingested through Opstrace errortracking microservice and sent to ClickHouse for insertion.

Queries will be be made from the GitLab Rails application to the Opstrace errortracking microservice for subsequent read requests.

**Describe each component of the new feature and enumerate what it does to support customer use cases.**

- Rails will serve as the main UI for interacting with error tracking features
- Dedicated opstrace errortracking service will handle ingestion of user client error data and proxying of query requests from GitLab Rails
- CHProxy will handle proxying of requests to single ClickHouse cluster, but serve as a stable load balancer as we scale out ClickHouse instances as-needed

**For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?**

If Opstrace cluster fails for connectivity reasons, query requests against analytical/observability workloads (error tracking queries, historical queries)
become inaccessible leading to degraded 5xx user experience on affected feature pages. This can be mitigated via replicas to maintain uptime

If Opstrace cluster fails for connectivity reasons, incoming data ingestion will fail and incoming data from sentry client libraries will not be persisted.

ClickHouse availability is inevitable. In the event that ClickHouse becomes unavailable, data loss occurs and we aim to keep the threshold within an acceptable rate. This can have significant business impact since observability data can be highly impactful for customers. In addition, observability data has a short lifespan on the user's side so a lack of buffering can result in a poor user experience.

Addition of a first-class database migration tool will be needed to safely handle DB migrations going forward, this will be implemented within https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1734
If a ClickHouse instance fails for storage-loss/corruption reasons, requests against analytical/observability workloads (error tracking queries, historical
queries) become inaccurate/misleading leading to degraded user experience on affected feature pages.

- Rails web servers unavailable
  - Data ingestion unaffected
- Opstrace cluster unavailable
  - Data ingestion fails, clients receive 500s on data submission
- ClickHouse instance unavailable
  - Data ingestion fails, clients receive 500s on data submission

**If applicable, explain how this new feature will scale and any potential single points of failure in the design.**

Current design relies on all ingestion traffic to be processed through Opstrace error tracking service. New component
is designed for high-write analytic workloads to reduce network pressure on Rails application.

Increased data throughput will not affect GitLab Rails availability as all ingestion occurs through Opstrace cluster.
Increased query usage may occur as more users are onboarded to feature availability (initially limited availability).

## Operational Risk Assessment

**What are the potential scalability or performance issues that may result with this change?**

Relying off external clients for incoming data can lead to huge spikes in unexpected traffic at any time. Processing incoming data through
avoids increased load on existing infrastructure by ingesting directly through Opstrace infrastructure.

- See https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1727 for implementation of rate-limiting/backoff headers.
- See https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1787 for enablement of Cloudflare for opstrace cluster.
- See https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1788 for documentation of autoscaling procedures for cluster.

**List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how the it will be impacted by a failure of that dependency.**

See **For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?**, above

- [ ] **Were there any features cut or compromises made to make the feature launch?**

Direct connectivity to ClickHouse via GitLab Rails was cut from initial scope to better support the high-data requirements of the error tracking feature. With future adoption of ClickHouse the gem and direct connection to ClickHouse from the Rails application may be reintroduced, which will require additional infrastructure changes as no direct peering is available between Rails app servers and ClickHouse instance.

**List the top three operational risks when this feature goes live.**

1. Data ingestion throughput reduces availability of opstrace cluster
1. Customer data loss via network connectivity or persistence issues with ingestion service
1. Significant storage costs due to error storage volume

- [ ] **What are a few operational concerns that will not be present at launch, but may be a concern later?**

At launch, the amount of error reporting traffic sent to opstrace in general is expected to be very low. As a result, any reliability issues are unlikely to be surfaced initially.

Initial configuration of Clickhouse cluster and CHProxy aim to support a single CH instance for error tracking. Future
support for separate instances (i.e. support for tracing workloads) will require a more complex CHProxy configuration.

By relying entirely off deployed golang errortracking service for ingestion we are not providing direct connectivity to the ClickHouse instance from rails webservers. If this connectivity is later added it will require a separate readiness and rollout.

**Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?**

Yes, the existing [integrated error tracking feature flag](https://gitlab.com/gitlab-org/gitlab/-/blob/94b0d2b9bb8b6b784f5a9813ab45fd8cac1a361c/config/feature_flags/development/integrated_error_tracking.yml)
is currently disabled by default. This flag will be continue to gate general availability of [the integrated error tracking feature](https://docs.gitlab.com/ee/operations/error_tracking.html#integrated-error-tracking).

ClickHouse connectivity will be managed via the [Opstrace error tracking API](https://gitlab.com/gitlab-org/opstrace/opstrace/-/merge_requests/1594) over HTTP.
This data layer will handle all connectivity to the datastore and be accessible via the GitLab Rails application, behind the above feature-flag. 

**Document every way the customer will interact with this new feature and how customers will be impacted by a failure of each interaction.**

Customers will configure their application using the sentry-sdk [to send error tracking data to GitLab.com](https://docs.gitlab.com/ee/operations/error_tracking.html#integrated-error-tracking) opstrace instance.
This will result in a variable volume of incoming events to be stored.

Customers will use the [error tracking list](https://docs.gitlab.com/ee/operations/error_tracking.html#error-tracking-list) to view and interact with stored vulnerabilities.

**As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?**

Worst case scenario is unavailability of production opstrace instance due to volume of incoming data. This is limited to the production
realm of opstrace infrastructure which will be peered to the GitLab Saas GCP project.
Authentication requests to verify projects between ingestion API and rails app relies on 2min cache durations to prevent overloading of internal API server, see [errorTrackingAPI ingress configuration](https://gitlab.com/gitlab-org/opstrace/opstrace/-/blob/e5d540ff1c4a5b52b16550678fc7e652402cfe2c/scheduler/controllers/cluster/errorTrackingAPI/ingress.go#L52)
Since error tracking will be the rollout feature for opstrace availability on GitLab SaaS, any failures will be scoped to the above feature flag.

## Database

**If we use a database, is the data structure verified and vetted by the database team?**

Yes, see https://gitlab.com/gitlab-org/gitlab/-/issues/358396

- [ ] **Do we have an approximate growth rate of the stored data (for capacity planning)?**

Ongoing, see https://gitlab.com/gitlab-org/gitlab/-/issues/360321

**Can we age data and delete data of a certain age?**

Proposed schema for error tracking data is partitioned by month allowing isolation of historic data.

Data retention strategies are being explored with https://gitlab.com/gitlab-org/gitlab/-/issues/358392

## Security and Compliance

- **Are we adding any new resources of the following type? (If yes, please list them here or link to a place where they are listed)**

  - **AWS Accounts/GCP Projects**

Yes, as of this writing we use dedicated AWS/GCP realms emulating the
three environments we provision resources in:
### GCP

|Environment|Project Name|Status|Details|
|---|---|---|---|
|Production|opstrace-prd-ac6ca492|Used|Hosting `observe.gitlab.com`|
|Staging|opstrace-stg-a8655950|Used|Hosting `staging.observe.gitlab.com`|
|Development|opstrace-dev-bee41fca|Used|Hosting development clusters|

### AWS

|Environment|Account ID|Status|Details|
|---|---|---|---|
|Production|607442520195|Not used yet|-|
|Staging|317624567080|Not used yet|-|
|Development|411567042229|Not used yet|-|


There's more infrastructure-specific details [here](https://gitlab.com/gitlab-org/opstrace/opstrace/-/blob/main/docs/architecture/infrastructure.md)

  - **New Subnets**
### GCP

|Environment|Project|VPC|Subnet|
|---|---|---|---|
|Production|opstrace-prd-ac6ca492|opstrace-prod-vpc|opstrace-prod-subnet|
|Staging|opstrace-stg-a8655950|opstrace-staging-vpc|opstrace-staging-subnet|


  - **VPC/Network Peering**

|Environment|Project|VPC|
|---|---|---|
|Production|opstrace-prd-ac6ca492|opstrace-prod-vpc|
|Staging|opstrace-stg-a8655950|opstrace-staging-vpc|

> There's no external VPC peering at the moment. The VPCs internally
> peer with GCP-specific networks - namely: `gke-ID`, `servicenetworking`.

  - **DNS names**

|Environment|Hosted DNS names|
|---|---|
|Production|`observe.gitab.com`|
|Staging|`observe.staging.gitlab.com`|

  - **Entry-points exposed to the internet (Public IPs, Load-Balancers, Buckets, etc...)**

For each environment, we have multiple ingress points. For example:

### Production

|Host URL|Endpoints|Component|
|---|---|---|
|`observe.gitab.com`|`/`|gatekeeper|
|`observe.gitlab.com`|`/errortracking/api/v1(/\|$)(.*)`|errortracking|

Each cluster can also host multiple "tenants" - groups of projects that
share a specific set of resources. Each tenant gets their own set of
ingress endpoints, each specific to a given subsystem.

|Host URL|Endpoints|Component|
|---|---|---|
|`observe.gitab.com`|`/$groupID`|observability UI|
|`observe.gitlab.com`|`/v1/errortracking/$groupID(/\|$)(.*)`|errortracking|
|`observe.gitlab.com`|`/v1/jaeger/$groupID`|jaeger UI|
|`observe.gitlab.com`|`/v1/traces/$groupID`|traces ingest|
|`observe.gitlab.com`|`/v1/jaegertraces/$groupID`|jaeger traces ingest|

  - **Other (anything relevant that might be worth mention)**

We document details of Gitlab Observability Platform [here](https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/architecture).

- **Secure Software Development Life Cycle (SSDLC)**
    - **Is the configuration following a security standard? (CIS is a good baseline for example)**

TBD

  - **All cloud infrastructure resources are labeled according to the [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) guidelines**

Pending https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1892

  - **Were the [GitLab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?**

TBD

  - **Do we have an automatic procedure to update the infrastructure (OS, container images, packages, etc...)**

As discussed in [this issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1650#note_918079928) we will be starting with stable ClickHouse versions and consider switching to LTS supported versions after release. In either case, regular security updates will be applied to both CHProxy and ClickHouse deployments.

Since all existing opstrace infrastructure is run within GKE all Kubernetes and OS updates are automatically handled by Google Cloud provider.

  - **Do we use IaC (Terraform) for all the infrastructure related to this feature? If not, what kind of resources are not covered?**

We use `terraform`/`terragrunt` to provision all infrastructure-related components. The definition for all underlying
resources and related `terraform` modules are available inside the
[project repository](https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/terraform/modules).

  - **Do we have secure static code analysis tools ([kics](https://github.com/Checkmarx/kics) or [checkov](https://github.com/bridgecrewio/checkov)) covering this feature's terraform?**

All our `terraform` modules are subject to static code analysis configured within our [CI pipeline definition](https://gitlab.com/gitlab-org/opstrace/opstrace/-/blob/main/.gitlab-ci.yml):

```
include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/SAST-IaC.latest.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml
```

  - **If there's a new terraform state:**
    - **Where is to terraform state stored, and who has access to it?**

All environments are provisioned via `Terraform` using definitions
inside `private` projects on `ops.gitlab.net`.

### GCP

|Environment|Repository|
|---|---|
|Production|[opstrace-prd-ac6ca492](https://ops.gitlab.net/opstrace-realm/environments/gcp/opstrace-prd-ac6ca492)|
|Staging|[opstrace-stg-a8655950](https://ops.gitlab.net/opstrace-realm/environments/gcp/opstrace-stg-a8655950)|
|Development|[opstrace-dev-bee41fca](https://ops.gitlab.net/opstrace-realm/environments/gcp/opstrace-dev-bee41fca)|

Each concerned pipeline stores `terraform` state inside Gitlab-provided HTTP state backends.

|Environment|Store|
|---|---|
|Production|`TF_ADDRESS: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/opstrace-prod-state`|
|Staging|`TF_ADDRESS: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/opstrace-staging-state`|

We push these changes via our CI pipeline, so only it has access to the
backing state. All project members however can have access to it as well
by setting it up locally.

  - **Does this feature add secrets to the terraform state? If yes, can they be stored in a secrets manager?**

Yes, we do manage related secrets via `terraform`, thereby storing them in the backing state.

  - **If we're creating new containers:**
    - **Are we using a distroless base image?**

Yes, all `Dockerfile` listed below:

|container|Dockerfile|
|---|---|
|`scheduler`|https://gitlab.com/gitlab-org/opstrace/opstrace/-/blob/main/scheduler/Dockerfile|
|`tenant-operator`|https://gitlab.com/gitlab-org/opstrace/opstrace/-/blob/main/tenant-operator/Dockerfile|
|`clickhouse-operator`|https://gitlab.com/gitlab-org/opstrace/opstrace/-/blob/main/clickhouse-operator/Dockerfile|
|`gatekeeper`|https://gitlab.com/gitlab-org/opstrace/opstrace/-/blob/main/gatekeeper/Dockerfile.gatekeeper|
|`errortracking`|https://gitlab.com/gitlab-org/opstrace/opstrace/-/blob/main/go/Dockerfile.errortracking-api|
|`tracing`|https://gitlab.com/gitlab-org/opstrace/opstrace/-/blob/main/go/Dockerfile.tracing|
|`UI`|https://gitlab.com/gitlab-org/opstrace/opstrace-ui/-/blob/main/Dockerfile|

  - **Do we have security scanners covering these containers?**
    - **`kics` or `checkov` for Dockerfiles for example**

We configure our CI pipelines with:
```
include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/SAST-IaC.latest.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml
```
  - **[GitLab's container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) scanner for vulnerabilities**

For `container scanning`, we configure it to dynamically add all container images used across the Platform.
```
# generate child pipeline content based on controller image tags and docker-images.json
image scan config:
  stage: image_scanning
  needs:
    - job: images
      optional: true
  script:
    - >
      cp -v ${PIPELINE_FILE}.header ${PIPELINE_FILE}
- >
      cat go/pkg/constants/docker-images.json |
      jq -r 'to_entries | .[] |
      "\(.key) image scan:
        extends: [.scanning_base]
        variables:
          DOCKER_IMAGE: \(.value)"' >> $PIPELINE_FILE
- >
      for i in $(sed -n -e 's/^\s\+-\s\+GOMOD_PATH:\s\+//p' .gitlab-ci.yml); do
        for IMG in $(make -sC $i print-docker-images); do
          echo -e "$i image scan:\n        extends: [.scanning_base]\n        variables:\n          DOCKER_IMAGE: $IMG\n" >> $PIPELINE_FILE
        done
      done
artifacts:
  paths:
    - $PIPELINE_FILE
  variables:
    PIPELINE_FILE: .gitlab/ci/container_scanning.yml
```
with `container_scanning.yml` generated & configured using [this file](https://gitlab.com/gitlab-org/opstrace/opstrace/-/blob/main/.gitlab/ci/container_scanning.yml.header).

- **Identity and Access Management**
  -  Are we adding any new forms of **Authentication** (New service-accounts, users/password for storage, OIDC, etc...)?

Gitlab Observability Platform uses `oauth` for authentication using
Gitlab itself as the provider. We delegate authentication handling to
Gitlab and use the corresponding user/project hierarchy.

  -  **Does it follow the least privilege principle?**

By leveraging the same authentication model Gitlab uses to allow regulated access to all underlying resources, we benefit from all the best principles implemented as a result, including least privilege principle.

On the infrastructure side, **only** team members have access to the `ops.gitlab.net` projects and the GCP/AWS realms - with each new addition going via an Access Request.

- **If we are adding any new Data Storage (Databases, buckets, etc...)**
  -  **What kind of data is stored on each system? (secrets, customer data, audit, etc...)**

All data persisted at our end can be described as monitoring/observability telemetry data such as error events, distributed traces, application metrics & logs generated by all users/applications of the platform.

  -  **How is data rated according to our [data classification standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html) (customer data is RED)**

While open for re-assessment, we believe all persisted data on Gitlab Observability Platform should be classified **RED** since it's all generated by customer application/services.

  -  **Is data it encrypted at rest? (If the storage is provided by a GCP service, the answer is most likely yes)**

No masking or modification of error events occur at present. All provided data is stored as received. All GCP persisted volumes are encrypted at rest by default.

Clickhouse [supports encryption](https://clickhouse.com/docs/en/sql-reference/statements/create/table/#create-query-encryption-codecs) at rest, however there's a limitation where indexed values will be stored as plaintext, see warning in docs. See https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1754 for field-level encryption discussion

Indexing will not be applied to error event payloads, which are planned to be encrypted at rest.

  -  **Do we have audit logs on data access?**

No, though all persisted data is only accessible via the application/APIs Gitlab Observability Platform makes available. While we do not have audit logs for data access yet, we do log all read requests made to our APIs.

- **Network security (encryption and ports should be clear in the architecture diagram above)**
  -  **Firewalls follow the least privilege principle (w/ network policies in Kubernetes or firewalls on cloud provider)**

Our services are **not** behind firewalls or regulated via explicit network policies.

  -  **Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)**

Our user-facing services are all defined with an `nginx-ingress` object which then configures the associated cloud load balancer.

Our domains are configured behind Cloudflare to enable DDoS protection and name resolution caching.

```
➜  ~ dig +short observe.staging.gitlab.com NS
maxim.ns.cloudflare.com.
nia.ns.cloudflare.com.
➜  ~ dig +short observe.gitlab.com NS
maxim.ns.cloudflare.com.
nia.ns.cloudflare.com.
```

From a database access perspective, all operations are limited by a "quota" specific to the database-user being used. All database-users are configured with a default of `1000 operations per hour, reads & writes combined`.

  -  **Is the service covered by a WAF (Web Application Firewall)**

The service is **not** covered by a WAF explicitly, but it does sit behind Cloudflare's DDoS protection as mentioned above.

- **Logging & Audit**
  - **Has effort been made to obscure or elide sensitive customer data in logging?**

No masking or modification of error events occur at present. All provided data is stored as received.

[Query masking rules are supported](https://clickhouse.com/docs/en/operations/server-configuration-parameters/settings/#query-masking-rules) as a means of protecting server logs but none are planned for initial release.

- **Compliance**
    - **Is the service subject to any regulatory/compliance standards? If so, detail which and provide details on applicable controls, management processes, additional monitoring, and mitigating factors.**

TBD

## Performance

- [ ] **Explain what validation was done following GitLab's [performance guidelines](https://docs.gitlab.com/ce/development/performance.html) please explain or link to the results below**
    * [Query Performer](https://docs.gitlab.com/ce/development/query_recorder.html)
    * [Sherlock](https://docs.gitlab.com/ce/development/profiling.html#sherlock)
    * [Request Profiling](https://docs.gitlab.com/ce/administration/monitoring/performance/request_profiling.html)

TBD

**Are there any potential performance impacts on the database when this feature is enabled at GitLab.com scale?**

By moving to ClickHouse the database load should remain largely unaffected for data ingestion and processing.

Stream classification of incoming data requests will rely on Postgresql database for validation of client key `error_tracking_client_keys` (see `ErrorTracking::ClientKey`) and project. There will be an increase in verification requests to [new internal auth endpoint](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/87894) leading to more load on main DB with expected volume TBD.

**Are there any throttling limits imposed by this feature? If so how are they managed?**

No additional limits will be imposed for the GitLab Rails application with this feature.

For incoming ingestion to the error tracking service, a fixed rate limit of 8 requests per second will be imposed for error collection. See [data research issue](https://gitlab.com/gitlab-org/gitlab/-/issues/360321#event-rates). This will be added with https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1727

For each error payload there is a [1mb maximum limit](https://docs.gitlab.com/ee/administration/instance_limits.html#amount-of-data-sent-from-sentry-through-error-tracking) in place instance-wide.

**If there are throttling limits, what is the customer experience of hitting a limit?**

Incoming errors will be rejected until window resets.

- [ ] **For all dependencies external and internal to the application, are there retry and back-off strategies for them?**

None at present.

**Does the feature account for brief spikes in traffic, at least 2x above the expected TPS?**

TBD

## Backup and Restore

- [ ] **Outside of existing backups, are there any other customer data that needs to be backed up for this product feature?**

Backup strategies are being explored with https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1653 and implemented via https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1673.

Existing error tracking data within Postgresql will need to be migrated, as covered in https://gitlab.com/gitlab-org/gitlab/-/issues/357473

- [ ] **Are backups monitored?**

TBD

- [ ] **Was a restore from backup tested?**

Not at this time

## Monitoring and Alerts

**Is the service logging in JSON format and are logs forwarded to logstash?**

There is [an outstanding MR for integrating structure logging into ClickHouse](https://github.com/ClickHouse/ClickHouse/pull/35694) which will be needed for JSON logs. At present, there is no such support.

Both ClickHouse and errortracking API logs will be monitored via stackdriving logging for now, see https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/15669#logging and https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1665

**Is the service reporting metrics to Prometheus?**

ClickHouse performance metrics will be made available via [prometheus exporter](https://clickhouse.com/docs/en/operations/server-configuration-parameters/settings/#server_configuration_parameters-prometheus) and alerts configured for monitoring query volume, disk usage, data volume to replicas, and active background jobs (merges).

We will want to establish error, saturation, and latency metrics for both the opstrace cluster and Clickhouse database itself.

Opstrace performance metric discussion is ongoing.

- [ ] **How is the end-to-end customer experience measured?**
- [ ] **Do we have a target SLA in place for this service?**
- [ ] **Do we know what the indicators (SLI) are that map to the target SLA?**
- [ ] **Do we have alerts that are triggered when the SLI's (and thus the SLA) are not met?**
- [ ] **Do we have troubleshooting runbooks linked to these alerts?**

Runbooks will live within https://gitlab.com/gitlab-org/opstrace/runbooks

- [ ] **What are the thresholds for tweeting or issuing an official customer notification for an outage related to this feature?**
- [ ] **do the oncall rotations responsible for this service have access to this service?**

Oncall rotation will be handled initially by the Monitor Observability team which will have direct access to the realm.

## Responsibility

**Which individuals are the subject matter experts and know the most about this feature?**

The Monitor Observability team will own and monitor both the feature rollout and initial infrastructure.

**Which team or set of individuals will take responsibility for the reliability of the feature once it is in production?**

The Monitor Observability team will own and monitor both the feature rollout and initial infrastructure.

**Is someone from the team who built the feature on call for the launch? If not, why not?**

Yes, the Observability will arrange for on-call team members to monitor the rollout and performance of the error tracking ClickHouse integration.

## Testing

**Describe the load test plan used for this feature. What breaking points were validated?**

High Level Steps:

| Step  | Description |
|-------|-------------|
| 1 | Enable error tracking API on staging.gitlab.com |
| 2 | Enable error tracking feature flag for staging load testing |
| 3 | Deploy error tracking API on production gitlab.com |
| 4 | Enable error tracking feature flag for production load testing of internal project |
| 5 | Enable error tracking feature flag for production, internal project availability |
| 6 | Enable error tracking feature flag for production, customer opt-in project availability |

Detailed Rollout Steps outlined in https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1728

- [ ] **For the component failures that were theorized for this feature, were they tested? If so include the results of these failure tests.**
- [ ] **Give a brief overview of what tests are run automatically in GitLab's CI/CD pipeline for this feature?**

Opstrace CI includes an end-to-end test pipeline for deploying ClickHouse operator and including basic inserts/reads.

